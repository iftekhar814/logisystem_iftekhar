import 'package:flutter/material.dart';

class TextStyles {
  static TextStyle title_big(BuildContext context) {
    return TextStyle(
      fontWeight: FontWeight.bold,
      color: Colors.black,
      fontSize: 30,
    );
  }

  static TextStyle title_med(BuildContext context) {
    return TextStyle(
      fontWeight: FontWeight.bold,
      color: Color(0xff2b2b2b),
      fontSize: 20,
    );
  }

  static TextStyle italic(BuildContext context) {
    return TextStyle(
      fontStyle: FontStyle.italic,
      color: Color.fromARGB(255, 8, 23, 85),
      fontSize: 14,
    );
  }

  static TextStyle normal(BuildContext context) {
    return TextStyle(
      fontSize: 14,
      color: Color.fromARGB(255, 0, 0, 0),
    );
  }

  static TextStyle error(BuildContext context) {
    return TextStyle(
      fontSize: 18,
      color: Color.fromARGB(255, 225, 27, 27),
    );
  }
}
