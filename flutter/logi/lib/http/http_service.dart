import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class HttpService {
  final String url = "";

  http.Client client = http.Client();
  String mainUrl = "https://9f59-103-131-88-131.in.ngrok.io";

  Future<String> login(String username, String password) async {
    final Body = jsonEncode({
      'username': username,
      'password': password,
    });

    final uri = Uri.parse('$mainUrl/api/login/');

    final Headers = {HttpHeaders.contentTypeHeader: 'application/json'};

    final response = await http.post(uri, headers: Headers, body: Body);
    return response.body.toString();
  }

  Future<String> Register(String username, String password) async {
    final Body = jsonEncode({
      'username': username,
      'password': password,
    });

    final uri = Uri.parse('$mainUrl/api/create/');

    final Headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    final response = await http.post(uri, headers: Headers, body: Body);
    return response.body.toString();
  }

  Future<String> Update(String username, String password) async {
    final body = jsonEncode({
      'username': username,
      'password': password,
    });

    final uri = Uri.parse('$mainUrl/api/update/$username');
    final headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    final response = await http.put(uri, headers: headers, body: body);
    return response.body.toString();
  }
}
