import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:logi/http/http_service.dart';
import 'package:logi/themes/TextStyles.dart';

class Forgot extends StatefulWidget {
  Forgot({Key? key}) : super(key: key);

  @override
  State<Forgot> createState() => _ForgotState();
}

class _ForgotState extends State<Forgot> {
  final TextEditingController usrName = TextEditingController();

  final TextEditingController pass = TextEditingController();

  final HttpService http_controller = HttpService();

  late Future<String> errorMsg;
  String errorDisplay = "...";

  void updateMsg() async {
    errorDisplay = await http_controller.Update(usrName.text, pass.text);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 3),
                child: Text(
                  "Forgot Password Form",
                  style: TextStyles.title_big(context),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 60),
                child: Text(
                  "Userrname / Email",
                  style: TextStyles.italic(context),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: SizedBox(
                  width: double.infinity,
                  child: TextField(
                    textAlign: TextAlign.center,
                    controller: usrName,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  "Password",
                  style: TextStyles.italic(context),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: SizedBox(
                  width: double.infinity,
                  child: TextField(
                    obscureText: true,
                    textAlign: TextAlign.center,
                    controller: pass,
                  ),
                ),
              ),
              Text(errorDisplay, style: TextStyles.error(context)),
              Padding(
                padding: EdgeInsets.only(top: 50),
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ElevatedButton(
                        child: Text("Reset Password"),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red,
                          padding: EdgeInsets.symmetric(
                              horizontal: 50, vertical: 20),
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          updateMsg();
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 10)),
                      ElevatedButton(
                        child: Text("Clear"),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          padding: EdgeInsets.symmetric(
                              horizontal: 50, vertical: 20),
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          usrName.clear();
                          pass.clear();
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
