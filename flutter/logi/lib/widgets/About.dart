import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:logi/themes/TextStyles.dart';

class About extends StatelessWidget {
  About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 3),
                child: Text(
                  "☮ About this App",
                  style: TextStyles.title_big(context),
                ),
              ),
              Divider(
                thickness: 3,
                color: Colors.black,
              ),
              Padding(
                padding: EdgeInsets.only(top: 60),
                child: Text(
                  "This app is made by Iftekhar Hyder",
                  style: TextStyles.title_med(context),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
