# LogiSystem_iftekhar

Registration and Login App(With Forgot Password Feature) made with flutter, & Backend is made using Spring Boot.

## important note
I have first run the Spring Boot Application in VSCode,
Then I have run ngrok to make it live on web.
After that I have taken that ngrok http url and put it in my flutter HttpService class mainUrl variable to communicate in flutter
application.
PostgreSQL has been used as the database.


## Best Regards
Iftekhar Hyder
