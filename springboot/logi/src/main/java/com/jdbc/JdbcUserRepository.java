package com.jdbc;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.models.User;
import com.user_repo.UserRepository;

@Repository
public class JdbcUserRepository implements UserRepository {
 
  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Override
  public List<User> getUsers(){

    return jdbcTemplate.query("SELECT * from users", BeanPropertyRowMapper.newInstance(User.class));

  }

  public boolean checkDuplicate(String username){
    List<User> list = jdbcTemplate.query("SELECT * from users", BeanPropertyRowMapper.newInstance(User.class));
    for(int i = 0; i <= list.size() - 1; i++){
     
      if(list.get(i).getUsername().equals(username)){
        return true;
      }else{
        
        continue;
      }
    }

    return false;


  }

  @Override
  public String save(User user) {
    
  if(checkDuplicate(user.getUsername()) == false){
    int run = jdbcTemplate.update("INSERT INTO users (username, password) VALUES(?,?)",
    new Object[] { user.getUsername(), user.getPassword() });
    return "User Registered Sucessfully! :)";
  }
   else if(checkDuplicate(user.getUsername()) == true){
    return "User Exists! :(";
    
   }else{
   
      return "Cannot register user, Invalid Entries! :(";
    
   
   }
  }
  @Override
  public String update(User user) {
    int run = jdbcTemplate.update("UPDATE users SET username=?, password=? WHERE username=?",
        new Object[] { user.getUsername(), user.getPassword(), user.getUsername() });
        if(run == 1){
          return "User Password Updated Sucessfully! :)";
         }else{
          return "Unable to Update User Password due to unknown username.";
         }
  }

  @Override
  public String checkPass(User user) {
    System.out.println(user.toString());

    List<User> list = jdbcTemplate.query("SELECT * from users", BeanPropertyRowMapper.newInstance(User.class));
    System.out.println(list.size());
    for(int i = 0; i <= list.size() - 1; i++){
      System.out.println(user.getUsername() + " " + user.getPassword());
     
      if(list.get(i).getUsername().equals(user.getUsername()) && list.get(i).getPassword().equals(user.getPassword())){
        return "User Logged in Sucessfully! :)";
      }else{
        
        continue;
      }
    }

    return "Invalid Username or Password! :(";
   

        
  }

  
}