package com.controllers;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.models.User;
import com.user_repo.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import com.controllers.UserController;

@RestController
@RequestMapping(value = "/api/")
public class UserController {

    
  @Autowired
  UserRepository userRepository;
  


  @GetMapping("/users")
  public ResponseEntity<String> getUsers(@RequestParam(required = false) String title) {
    
     List<User> l =(List<User>) userRepository.getUsers();
     String msg = "";
     for(User u : l){
        msg += u.getUsername() + "\n";
     }

     
   
      return new ResponseEntity<String>(msg, HttpStatus.OK);
   
  }

  @PostMapping("/login")
  public ResponseEntity<String> login(@RequestBody User user) {
    String msg = userRepository.checkPass(user);
   
      return new ResponseEntity<String>(msg, HttpStatus.OK);
   
  }
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public ResponseEntity<String> createUser(@RequestBody User user) {
   
      String msg = userRepository.save(new User(user.getUsername(), user.getPassword()));
      return new ResponseEntity<String>(msg, HttpStatus.OK);
    
     
    
  }
  @RequestMapping(value = "/update/{username}", method = RequestMethod.PUT)
  public ResponseEntity<String> updateUser(@PathVariable("username") String id, @RequestBody User user) {
    String msg = userRepository.update(user);
    return new ResponseEntity<String>(msg, HttpStatus.OK);
    
  }

  


    // public static void main(String[] args){  
    
    //     SpringApplication.run(UserController.class, args);
    // }  
  
 
}