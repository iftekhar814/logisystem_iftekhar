package com.models;

public class User {
  
  private String username;
  private String password;

  public User() {
  }
  
  public User(String username, String password) {
    
  this.username = username;
  this.password = password;
  }
  
  
  public void setUsername(String usrName) {
    this.username = usrName;
  }

  public String getUsername(){
    return username;
  }
  
  public String getPassword() {
    return password;
  }
  
  public void setPassword(String p) {
    this.password = p;
  }

  @Override
  public String toString() {
    return "User [password=" + password + ", username=" + username + "]";
  }
  
  
}
