package com.user_repo;

import java.util.List;

import com.models.User;


public interface UserRepository {

  String save(User book);
  String update(User book); 
  String checkPass(User book);
  List<User> getUsers();
}
